import Vue from "vue";
import axios from "axios";

function WeatherQuery( query ) {

	const url = "http://api.openweathermap.org/data/2.5/forecast"
	const appid = "5121f13fd90c7f33f764109bcfc0c9fd"

	/** DEFAULTS */
	query.units = query.units || "metric"
	query.mode = query.mode || "json"

	this.getQuery = function() {
		let params = ""

		for ( let param in query ) {
			params += param + "=" + query[ param ] + "&";
		}

		return `${ url }?${ params }appid=${ appid }`
	}
}
 
/**
 * Filter daily list
 */
function filterDailyList() {
	const { country, cityISO } = vm.weatherFilters
	const queryUrl = new WeatherQuery({ q: `${country},${cityISO}` }).getQuery()

	vm.loading = true;

	axios.get( queryUrl ).then( updateWidget ).catch(
		function( response ) {
			vm.queryError = response.message
			vm.loading = false;
		}
	)
}

/**
 * Update Weather widget
 */
function updateWidget( response ) {
	if( response.status === 200 ){
		vm.dailyList = response.data.list	
		vm.queryError = false	
	}

	vm.loading = false;
}

/**
 * View-Model
 */
const vm = new Vue({
	el: "#weatherWidget",
	filters: {
	  formatDate: date => date.split(' ')[ 0 ]
	},
	data: {
		dailyList: [],
		queryError: false,
		loading: true,
		layout: "list-layout",
		weatherFilters: {
			country: "London",
			cityISO: "us"
		}
	},
	methods: { filterDailyList },
	mounted: function(){
		window.addEventListener("keypress", function( event ){			

			const isInput = event.target.nodeName === "INPUT";

			switch ( event.charCode ) {

				case 103:
					if( !isInput ) vm.layout = "grid-layout";
					break;

				case 108:
					if( !isInput ) vm.layout = "list-layout";
					break;
					
			}
			if ( event.charCode === 103 ) ;
			console.log(event)
		})
	}
})


/**
 * Init.
 */

document.addEventListener('DOMContentLoaded', function(){ 	
    vm.filterDailyList()
}, false);
