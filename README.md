# BadrIt evaluation task.

#### **[ run in order ]**


### Install dependencies
```sh
$ npm install
```

---
### [ * important hack * ]
Vuejs includes the run-time version and template compilation isn't included in run-time. this hack will force it to include the standalone version

**After installation navigate to:**

"node_modules/vue/package.json"

**And change module path to:**

"module": "dist/vue.common.js"


Another way is to include a CDN version of vuejs, but don't forget to remove import vue from "vue" at "index.js"


---

### Build command
```sh
$ npm run build
```

### run browser sync
```sh
$ npm run bsync
```

### compile styles
```sh
$ npm run sass
```

---
### TODO:
 - Routing & state management
 - pagination
 - performance optimization
 - Tons of UI/UX improvements