import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import replace from 'rollup-plugin-replace'

const plugins = [
	/** resolve node modules  */
	resolve({
		browser: true
	}),
	/** convert CommonJS modules to ES6 */
	commonjs(),
	/** transpile es2015 */
	babel({
	  "presets":
	  [["es2015", { "modules": false }]],
	  
	  "plugins":
	  ["external-helpers"]
	}),
	/** replace ENV variables */
	replace({
	      'process.env.NODE_ENV': JSON.stringify('development'),
	      'process.env.VUE_ENV': JSON.stringify('browser')
    }),
	/** uglify */
	uglify()
]

export default {
	input: "app/index.js",
	output: {
		name: "badrBundle",
		file: "build/js/bundle.js",
		format: "iife",
		sourcemap: "inline"		
	},
	plugins,
	globals: {
		'vue': 'Vue',
		'axios': 'axios'
	}
}